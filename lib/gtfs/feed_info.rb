module GTFS
  class FeedInfo
    include GTFS::Model

    column_prefix :feed_

    has_attributes :feed_publisher_name, :feed_publisher_url, :feed_lang, :feed_start_date, :feed_end_date, :feed_version
    set_attributes_optional :feed_start_date, :feed_end_date, :feed_version
    attr_accessor *attrs

    collection_name :feed_infos
    uses_filename 'feed_info.txt'

    def start_date
      @parsed_start_date ||= Date.parse(@start_date) if @start_date
    end

    def end_date
      @parsed_end_date ||= Date.parse(@end_date) if @end_date
    end

    def self.parse_feed_infos(data, options={})
      return parse_models(data, options)
    end
  end
end
