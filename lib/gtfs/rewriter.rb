module GTFS
  class Visitor

    def initialize(source)
      @source = source
    end

    attr_reader :source

    def visit
      [
        source.routes,
        source.trips,
        source.stop_times,
        source.calendars,
        source.calendar_dates,
        source.shapes,
        # children first
        source.stops.select { |stop| ! stop.parent_station.nil? },
        # then parents
        source.stops.select { |stop| stop.parent_station.nil? },
        source.agencies,
        source.attributions
      ].each do |collection|
        collection.each { |resource| call(resource) }
      end
    end

    def resource_type(resource_class)
      resource_class.name.to_s.split('::').last.
        gsub(/([a-z]+)([A-Z0-9])/, '\1_\2').downcase.
        gsub(/ies$/,'y').gsub(/s$/,'').to_sym
    end

    def resource_class_methods
      @resource_class_methods ||= {}
    end

    def find_resource_class_method(resource_class)
      candidate = "each_#{resource_type(resource_class)}"
      respond_to?(candidate) ? candidate : "each"
    end

    def resource_method(resource_class)
      resource_class_methods[resource_class] ||= find_resource_class_method(resource_class)
    end

    def call(resource)
      send resource_method(resource.class), resource
    end

    def each(resource)

    end

  end

  class UniquenessTarget

    def initialize(target)
      @target = target
    end

    def collections
      @collections ||= Hash.new { |h,k| h[k] = Collection.new k, @target }
    end

    COLLECTION_NAMES = %i{routes trips stop_times calendars calendar_dates stops agencies shapes attributions}

    def method_missing(method, *arguments)
      if COLLECTION_NAMES.include? method
        collections[method]
      else
        @target.send method, *arguments
      end
    end

    class Collection

      def initialize(name, target)
        @name, @target = name, target
      end

      attr_reader :name, :targets

      def pushed_identifiers
        @pushed_identifiers ||= Set.new
      end

      def resource_id(resource)
        case resource
        when CalendarDate
          [resource.service_id, resource.date]
        when StopTime
          [resource.trip_id, resource.stop_sequence]
        else
          resource.id if resource.respond_to?(:id)
        end
      end

      def push(resource)
        identifier = resource_id(resource)
        if !identifier.nil? && pushed_identifiers.add?(identifier).nil?
          return
        end

        @target.send(name) << resource
      end
      alias << push
    end

  end

  class MultipleTarget

    def initialize(targets)
      @targets = targets
    end

    def collections
      @collections ||= Hash.new { |h,k| h[k] = Collection.new k, @targets }
    end

    COLLECTION_NAMES = %i{routes trips stop_times calendars calendar_dates stops agencies shapes attributions}

    def method_missing(method, *arguments)
      if COLLECTION_NAMES.include? method
        collections[method]
      else
        super
      end
    end

    class Collection

      def initialize(name, targets)
        @name, @targets = name, targets
      end

      attr_reader :name, :targets

      def push(resource)
        @targets.each do |target|
          target.send(name) << resource
        end
      end
      alias << push

    end

  end

  class VoidTarget

    def collections
      @collections ||= Hash.new { |h,k| h[k] = Collection.new k, @targets }
    end

    COLLECTION_NAMES = %i{routes trips stop_times calendars calendar_dates stops agencies shapes attributions}

    def method_missing(method, *arguments)
      if COLLECTION_NAMES.include? method
        void_collection
      else
        super
      end
    end

    def void_collection
      @void_collection ||= Collection.new
    end

    class Collection

      def push(resource)
      end
      alias << push

    end

  end

  class Rewriter

    def initialize(source_file, target_for: nil)
      @source_file = source_file
      @target_for = target_for
    end

    attr_reader :source_file

    def source
      @source ||= GTFS::Source.build source_file, strict: false
    end

    def targets
      @targets ||= {}
    end

    #
    # For example:
    #
    # route_ids = associations[:route_id]
    # multiple_targets route_ids.map { |route_id| target target_file(route_id) })
    #
    # target "custom.zip"
    #
    # nil
    #
    def target_for(resource, associations = {})
      if @target_for
        instance_exec resource, associations, &@target_for
      else
        raise "To be implemented"
      end
    end

    def void_target
      @void_target ||= VoidTarget.new
    end

    def multiple_targets(targets)
      MultipleTarget.new(targets)
    end

    def target_file(slug)
      slug = slug.gsub(/[^0-9a-z]/i,'_')
      source_file.gsub(/.zip/,"-#{slug}.zip")
    end

    def create_target(file)
      UniquenessTarget.new(GTFS::Target.new(file))
    end

    def target(file)
      targets[file] ||= create_target(file)
    end

    def rewrite
      Visitor.new(self).visit
      close
    end

    def close
      targets.values.each(&:close)
    end

    class Visitor < ::GTFS::Visitor

      def initialize(rewriter)
        super rewriter.source
        @rewriter = rewriter

        @route_ids ||= Hash.new { |h,k| h[k] = [] }
      end

      def target_for(resource)
        @rewriter.target_for resource, route_ids: route_ids(resource), agency_ids: agency_ids(resource)
      end

      def resource_array(resource)
        unless resource.is_a?(Array)
          [resource_type(resource.class), resource.id]
        else
          resource
        end
      end

      def register_route_id(resource, route_id)
        @route_ids[resource_array(resource)] << route_id
      end

      def register_route_id_like(resource, other_resource)
        @route_ids[resource_array(resource)].concat route_ids(other_resource)
      end

      def route_agency_id(route_id)
        @route_agency_ids ||= source.routes.map do |route|
          [route.id, route.agency_id]
        end.to_h

        @route_agency_ids[route_id]
      end

      def agency_ids(resource)
        if resource.is_a? GTFS::Agency
          [resource.id]
        else
          route_ids(resource).map { |route_id| route_agency_id(route_id) }
        end
      end

      def route_ids(resource)
        if resource.is_a? GTFS::Route
          [resource.id]
        elsif resource.is_a?(GTFS::Agency) && source.agencies.count == 1
          all_route_ids
        else
          @route_ids[resource_array(resource)]
        end
      end

      def each_route(route)
        register_route_id [:agency, route.agency_id], route.id
        target_for(route).routes << route
      end

      def each_trip(trip)
        register_route_id trip, trip.route_id
        register_route_id [:calendar, trip.service_id], trip.route_id
        register_route_id [:calendar_date, trip.service_id], trip.route_id
        register_route_id [:shape, trip.shape_id], trip.route_id

        target_for(trip).trips << trip
      end

      def each_stop_time(stop_time)
        register_route_id_like [:stop, stop_time.stop_id], [:trip, stop_time.trip_id]
        target_for([:trip, stop_time.trip_id]).stop_times << stop_time
      end

      def each_shape(shape)
        target_for(shape).shapes << shape
      end

      def each_calendar(calendar)
        target_for(calendar).calendars << calendar
      end

      def each_calendar_date(calendar_date)
        target_for(calendar_date).calendar_dates << calendar_date
      end

      def each_stop(stop)
        if stop.parent_station
          register_route_id_like [:stop, stop.parent_station], stop
        end
        target_for(stop).stops << stop
      end

      def each_agency(agency)
        target_for(agency).agencies << agency
      end

      def each_attribution(attribution)
        target_for(attribution).attributions << attribution
      end

      def all_route_ids
        @route_ids.values.flatten.uniq
      end

    end

  end
end
