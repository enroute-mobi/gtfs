# frozen_string_literal: true

module GTFS
  class Service
    extend Forwardable

    def initialize(service_id: nil, calendar: nil, calendar_dates: [])
      @service_id = service_id || calendar&.service_id || calendar_dates&.first&.service_id
      @calendar = calendar
      @calendar_dates = calendar_dates || []
    end
    
    attr_reader :service_id, :calendar, :calendar_dates

    [:monday?, :tuesday?, :wednesday?, :thursday?, :friday?, :saturday?, :sunday?].each do |method|
      define_method method do 
        return false unless calendar

        calendar.send(method) 
      end
    end

    [:start_date, :end_date, :date_range].each do |method|
      define_method method do 
        calendar&.send(method) 
      end
    end
  end
end