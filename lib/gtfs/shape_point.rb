module GTFS
  class ShapePoint
    include GTFS::Model

    column_prefix :shape_

    has_attributes :shape_id, :shape_pt_lat, :shape_pt_lon, :shape_pt_sequence, :shape_dist_traveled
    set_attributes_optional :shape_dist_traveled
    attr_accessor(*attrs)

    collection_name :shape_points
    uses_filename 'shapes.txt'

    alias shape_id id
    alias shape_id= id=

    def latitude 
      pt_lat&.to_f
    end
    alias latitude= pt_lat=

    def longitude 
      pt_lon&.to_f
    end
    alias longitude= pt_lon=

    alias sequence pt_sequence
    alias sequence= pt_sequence=

    def self.parse_shape_points(data, options={})
      return parse_models(data, options)
    end
  end
end
