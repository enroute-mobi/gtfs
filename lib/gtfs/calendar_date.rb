module GTFS
  class CalendarDate
    include GTFS::Model

    has_attributes :service_id, :date, :exception_type
    attr_accessor *attrs

    alias id service_id

    ADDED = '1'
    REMOVED = '2'

    def added?
      exception_type == ADDED
    end
    alias included? added?

    def removed?
      exception_type == REMOVED
    end
    alias excluded? removed?

    def ruby_date
      Date.parse date if date
    rescue Date::Error
      nil
    end

    collection_name :calendar_dates
    uses_filename 'calendar_dates.txt'

    def self.parse_calendar_dates(data, options={})
      return parse_models(data, options)
    end
  end
end
