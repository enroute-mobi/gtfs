module GTFS
  class Attribution
    include GTFS::Model

    has_attributes :attribution_id, :agency_id, :route_id, :trip_id, :organization_name, :is_producer, :is_operator, :is_authority, :attribution_url, :attribution_email, :attribution_phone
    set_attributes_optional :attribution_id, :agency_id, :route_id, :trip_id, :is_producer, :is_operator, :is_authority, :attribution_url, :attribution_email, :attribution_phone
    attr_accessor *attrs

    alias id attribution_id

    collection_name :attributions
    uses_filename 'attributions.txt'

    def operator?
      is_operator == '1'
    end

    def producer?
      is_producer == '1'
    end

    def authority?
      is_authority == '1'
    end

    def operator=(value)
      self.is_operator = value ? '1' : '0'
    end

    def self.parse_attributions(data, options={})
      return parse_models(data, options)
    end
  end
end
