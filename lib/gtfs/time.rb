module GTFS
  class Time
    attr_reader :hour, :minute, :second, :day_offset
    def initialize(hour = 0, minute = 0, second = 0, day_offset: 0)
      if hour > 23
        day_offset = hour / 24
        hour = hour % 24
      end
      @hour, @minute, @second, @day_offset = hour, minute, second, day_offset

      freeze
    end

    def hour_with_day_offset
      hour + day_offset * 24
    end

    def from_day_offset offset
      self.class.new hour, minute, second, day_offset: (day_offset-offset)
    end

    def self.create(time = nil, attributes = nil)
      attributes ||= {}

      %i{hour minute min second sec day_offset}.each do |attribute|
        attributes[attribute] = time.send(attribute) if time.respond_to?(attribute)
      end

      if minute = attributes.delete(:min)
        attributes[:minute] = minute
      end
      if second = attributes.delete(:sec)
        attributes[:second] = second
      end

      new attributes.fetch(:hour), attributes[:minute], attributes[:second], day_offset: attributes.fetch(:day_offset, 0)
    end

    OUTPUT_FORMAT = "%.2d:%.2d:%.2d".freeze
    def to_s
      OUTPUT_FORMAT % [ hour_with_day_offset, minute, second ]
    end

    INPUT_FORMAT = /(\d{1,2}):(\d{1,2})(:(\d{1,2}))?/
    def self.parse(definition)
      if definition.to_s =~ INPUT_FORMAT
        new(*[$1, $2, $4].map(&:to_i))
      end
    end

    def ==(other)
      other &&
        hour == other.hour &&
        minute == other.minute &&
        second == other.second &&
        day_offset == other.day_offset
    end

    include Comparable

    def <=>(other)
      [ day_offset, hour, minute, second ] <=>
        [ other.day_offset, other.hour, other.minute, other.second ]
    end
  end
end
