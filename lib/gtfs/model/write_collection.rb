module GTFS
  module Model
    class WriteCollection
      def initialize(klass)
        @collection = AutoCollection.new(klass)
        @klass = klass
        @unused_attrs = @klass.optional_attrs_objects.dup
      end

      attr_reader :unused_attrs, :collection

      def push(data)
        object =
          unless data.is_a? @klass
            @klass.new(data)
          else
            data
          end

        @unused_attrs.delete_if {|a| !object.send(a.name).nil? }
        @collection << object
      end
      alias << push

      def empty?
        @collection.empty?
      end

      include Enumerable
      def each(&block)
        @collection.each(&block)
      end

      def array_to_csv(csv)
        csv << @klass.csv_attrs - @unused_attrs.map(&:csv_name)

        used_attrs = @klass.attrs - @unused_attrs.map(&:name)
        @collection.each do |o|
          csv << o.to_csv(used_attrs)
        end
      end
    end

    class AutoCollection
      include Enumerable

      DEFAULT_SIZE = 50000

      def self.maximum_size(klass)
        if FileCollection.support?(klass)
          DEFAULT_SIZE
        end
      end

      def initialize(klass, maximum_size: nil)
        maximum_size ||= self.class.maximum_size(klass)
        @collection = MemoryCollection.new maximum_size
      end

      attr_reader :collection

      def push(model)
        collection.push model
        check_collection
      end
      alias << push

      def check_collection
        if collection.full?
          switch_to_file
        end
      end

      def switch_to_file
        new_collection = FileCollection.new
        collection.each do |model|
          new_collection.push model
        end

        @collection = new_collection
      end

      def empty?
        collection.empty?
      end

      def each(&block)
        collection.each(&block)
      end

    end

    class MemoryCollection
      include Enumerable

      def initialize(maximum_size = nil)
        @maximum_size = maximum_size
      end

      attr_reader :maximum_size

      def full?
        !maximum_size.nil? && models.size > maximum_size
      end

      def push(model)
        models << model
      end

      def empty?
        models.empty?
      end

      def models
        @models ||= []
      end

      def each(&block)
        models.each(&block)
      end

    end

    class FileCollection
      include Enumerable

      def self.support?(klass)
        message_pack_factory.type_registered? klass
      end

      def self.message_pack_factory
        GTFS::MessagePack::Factory.instance
      end

      def initialize
        @empty = true
      end

      def full?
        false
      end

      def push(model)
        @empty = false
        packer.write model
      end

      def empty?
        @empty
      end

      def each
        packer.flush
        file.rewind

        unpacker = self.class.message_pack_factory.unpacker(file)

        loop do
          begin
            yield unpacker.read
          rescue EOFError
            break
          end
        end
      end

      def file
        @file ||= Tempfile.new('gtfs', binmode: true)
      end

      def packer
        @packer ||= self.class.message_pack_factory.packer(file)
      end

    end


  end
end
