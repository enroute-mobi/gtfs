require 'csv'

module GTFS
  module Model
    module ClassMethods

      #####################################
      # Getters for class variables
      #####################################

      def prefix
        self.class_variable_get('@@prefix')
      end

      def optional_attrs_objects
        self.class_variable_get('@@attributes').select{|a| a.optionnal}
      end

      def required_attrs
        self.class_variable_get('@@attributes').reject{|a| a.optionnal}.map(&:name)
      end

      def attrs
        @attrs ||= self.class_variable_get('@@attributes').map(&:name)
      end

      def csv_attrs
        self.class_variable_get('@@attributes').map(&:csv_name)
      end


      #####################################
      # Helper methods for setting up class variables
      #####################################

      def has_attributes(*attrs)
        attrs.each {|a| self.class_variable_get('@@attributes') << GTFSAttribute.new(a.to_s.gsub(/^#{prefix}/, '').to_sym, a) }
      end

      def set_attributes_optional(*attrs)
        self.class_variable_get('@@attributes').map { |a| a.set_optionnal if attrs.include?(a.csv_name) }
      end

      def column_prefix(prefix)
        self.class_variable_set('@@prefix', prefix)
      end

      def collection_name(collection_name)
        self.define_singleton_method(:name) {collection_name}
        self.define_singleton_method(:collection_name) { collection_name }

        self.define_singleton_method(:singular_name) {
          self.to_s.split('::').last.
          gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
          gsub(/([a-z\d])([A-Z])/,'\1_\2').
          tr("-", "_").downcase
        }
      end

      def uses_filename(filename)
        self.define_singleton_method(:filename) {filename}
      end

      def each(filename)
        return unless File.exist?(filename)

        CSV.foreach(filename, :headers => true) do |row|
          yield parse_model(row.to_hash)
        end
      end

      # TODO: Very ugly code. See GTFS-13
      def parse_model(attr_hash)
        unprefixed_attr_hash = {}

        attr_hash.each do |key, val|
          next unless key && val

          val.strip!
          next if val.empty?

          key = key.to_s.gsub(/[^a-zA-Z_]/, '').gsub(/^#{prefix}/, '')

          unprefixed_attr_hash[key] = val
        end

        new unprefixed_attr_hash
      end

      def parse_models(data, options={})
        return [] if data.nil? || data.empty?

        models = []
        CSV.parse(data, headers: true, liberal_parsing: true) do |row|
          model = parse_model(row.to_hash)
          models << model if options[:strict] == false || model.valid?
        end
        models
      end

      def new_write_collection
        WriteCollection.new(self)
      end

      def generate_csv(&block)
        CSV.generate do |csv|
          c = WriteCollection.new(self)
          yield c
          c.array_to_csv csv
        end
      end

      def to_csv(value)
        case value
        when Date
          value.strftime("%Y%m%d")
        else
          value&.to_s
        end
      end

      #####################################
      # Msgpack methods
      #####################################

      def from_msgpack_ext(data)
        data_array = ::MessagePack.unpack(data)

        new.tap do |model|
          data_array.each_with_index do |value, index|
            attribute = attrs[index]
            model.send "#{attribute}=", value if value.size > 0
          end
        end
      end
    end
  end
end
