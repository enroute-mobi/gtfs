module GTFS
  class OfflineSort

    def self.chunk_input_output_class
      MessagePack::OfflineSort

      # To disable MessagePack chunck
      # ::OfflineSort::Chunk::InputOutput::Marshal
    end

    def self.sort(enumerable, options = {}, &block)
      ::OfflineSort.sort enumerable, **options.merge(chunk_input_output_class: chunk_input_output_class), &block
    end
  end


  module MessagePack
    class Factory < ::MessagePack::Factory

      def initialize
        super

        register_type 0x01, GTFS::Trip
        register_type 0x02, GTFS::StopTime
        register_type 0x03, GTFS::ShapePoint
        register_type 0x04, GTFS::Calendar
        register_type 0x05, GTFS::CalendarDate
      end

      def self.instance
        @instance ||= new
      end

    end

    class OfflineSort < ::OfflineSort::Chunk::InputOutput::MessagePack

      def initialize(io)
        super

        @packer = Factory.instance.packer(io)
        @unpacker = Factory.instance.unpacker(io)
      end

    end
  end
end
