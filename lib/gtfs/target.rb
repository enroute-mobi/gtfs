require 'zip'

module GTFS
  class Target
    ENTITIES = [GTFS::Agency, GTFS::Stop, GTFS::Route, GTFS::Trip, GTFS::StopTime,
                GTFS::Calendar, GTFS::CalendarDate, GTFS::ShapePoint, GTFS::FareAttribute,
                GTFS::FareRule, GTFS::Frequency, GTFS::Transfer, GTFS::FeedInfo, GTFS::Attribution]

    attr_reader :zip_file_path

    def initialize(zip_file_path)
      @zip_file_path = zip_file_path

      ENTITIES.each do |entity|
        instance_variable_set("@#{entity.name}_csv", entity.new_write_collection )

        define_singleton_method entity.name do
          instance_variable_get("@#{entity.name}_csv")
        end
      end
    end

    def shapes
      @shapes ||= Shapes.new(self)
    end

    def add(resource)
      send(resource.class.collection_name) << resource
    end
    alias << add

    class Shapes

      def initialize(target)
        @target = target
      end
      attr_reader :target

      def push(shape)
        shape.points.each_with_index do |shape_point, sequence|
          shape_point.shape_id = shape.id
          shape_point.sequence = sequence

          target.shape_points << shape_point
        end
      end
      alias << push

    end

    def self.open(zip_file_path)
      target = Target.new zip_file_path
      yield target
      target.close
    end

    def close
      Zip::File.open(zip_file_path, Zip::File::CREATE) do |zipfile|
        ENTITIES.each do |entity|
          collection = instance_variable_get("@#{entity.name}_csv")
          unless collection.empty?
            entity_csv = CSV.generate do |csv|
              collection.array_to_csv csv
            end

            zipfile.get_output_stream("#{entity.filename}") { |f| f.puts entity_csv }
          end
        end
      end
    end

  end
end
