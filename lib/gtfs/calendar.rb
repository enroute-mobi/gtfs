module GTFS
  class Calendar
    include GTFS::Model

    has_attributes :service_id, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday, :sunday, :start_date, :end_date
    attr_accessor *attrs

    %i[monday tuesday wednesday thursday friday saturday sunday].each do |day|
      define_method "#{day}?" do 
        send(day) == '1'
      end
    end

    alias id service_id

    # TODO Replace string start_date and end_date by Ruby dates

    def date_range
      return nil unless @start_date && @end_date

      from = Date.parse(@start_date)
      to = Date.parse(@end_date)

      return nil if from > to

      Range.new from, to 
    rescue Date::Error
      nil
    end

    collection_name :calendars
    uses_filename 'calendar.txt'

    def self.parse_calendars(data, options={})
      return parse_models(data, options)
    end
  end
end
