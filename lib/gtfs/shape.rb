module GTFS
  class Shape

    attr_accessor :id

    def initialize(attributes = {})
      attributes.each { |k,v| send "#{k}=", v }
    end

    def points
      @points ||= []
    end

  end
end
