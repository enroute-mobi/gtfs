module GTFS
  class Source

    ENTITIES = [GTFS::Agency, GTFS::Stop, GTFS::Route, GTFS::Trip, GTFS::StopTime,
                GTFS::Calendar, GTFS::CalendarDate, GTFS::ShapePoint, GTFS::FareAttribute,
                GTFS::FareRule, GTFS::Frequency, GTFS::Transfer, GTFS::FeedInfo, GTFS::Attribution]

    DEFAULT_OPTIONS = {strict: true}

    DEFAULT_MAX_SIZE = 1024**3 * 2

    attr_accessor :source, :archive, :options

    def initialize(source, opts={})
      raise 'Source cannot be nil' if source.nil?

      @tmp_dir = Dir.mktmpdir
      ObjectSpace.define_finalizer(self, self.class.finalize(@tmp_dir))

      @source = source
      load_archive(@source)

      @options = DEFAULT_OPTIONS.merge(opts)
    end

    def self.finalize(directory)
      proc {FileUtils.rm_rf(directory)}
    end

    def extract_to_cache(source_path)
      Zip::File.open(source_path) do |zip|
        ENTITIES.map(&:filename).each do |filename|
          entry = zip.glob("**/#{filename}").first
          next unless entry

          raise 'File too large when extracted' if entry.size > DEFAULT_MAX_SIZE

          tmp_file = File.join(@tmp_dir, '/', filename)
          zip.extract(entry.name, tmp_file)
        end
      end
    end

    def load_archive(source)
      raise 'Cannot directly instantiate base GTFS::Source'
    end

    def self.build(data_root, opts={})
      if File.exist?(data_root)
        LocalSource.new(data_root, opts)
      else
        URLSource.new(data_root, opts)
      end
    end

    def entries
      Dir.entries(@tmp_dir)
    end

    ENTITIES.each do |entity|
      define_method entity.name.to_sym do
        parse_file entity.filename do |f|
          entity.send("parse_#{entity.name}".to_sym, f.read, options)
        end
      end

      define_method "each_#{entity.singular_name}".to_sym do |&block|
        entity.each(File.join(@tmp_dir, entity.filename)) { |model| block.call model }
      end

      define_method "#{entity.name}_count".to_sym do |&block|
        file = File.join(@tmp_dir, entity.filename)
        if File.exist?(file)
          File.foreach(file).inject(-1) {|c, _| c+1}
        else
          0
        end
      end
    end

    def trips
      enum_for(:each_trip)
    end

    def stop_times
      enum_for(:each_stop_time)
    end

    # Provide to the given bloc each trip with associated stop times sorted by sequence
    def each_trip_with_stop_times
      valid_trips = trips.reject { |trip| trip.id.nil? }
      sorted_trips = OfflineSort.sort(valid_trips, chunk_size: 150000, &:id)

      valid_stop_times = stop_times.reject { |stop_time| stop_time.trip_id.nil? || stop_time.sequence.nil? }
      sorted_stop_times = OfflineSort.sort(valid_stop_times, chunk_size: 250000) do |stop_time|
        [ stop_time.trip_id, stop_time.sequence ]
      end

      next_stop_time =
        begin
          sorted_stop_times.next
        rescue StopIteration
          # no valid Stop Time is present
          nil
        end

      sorted_trips.each do |trip|
        stop_times = []

        # next_stop_time is nil when no more Stop Time is available
        unless next_stop_time.nil?
          begin
            while next_stop_time.trip_id < trip.id
              # The current Stop Time is associated to a missing Trip
              next_stop_time = sorted_stop_times.next
            end

            while next_stop_time.trip_id == trip.id
              # Add Stop Time to current Trip
              stop_times << next_stop_time
              next_stop_time = sorted_stop_times.next
            end
          rescue StopIteration
            # No more Stop Time
            next_stop_time = nil
          end
        end

        yield trip, stop_times
      end
    end

    def shape_points
      enum_for(:each_shape_point)
    end

    def each_shape
      valid_shape_points = shape_points.reject { |shape_point| shape_point.id.nil? || shape_point.pt_sequence.nil? }
      sorted_shape_points = OfflineSort.sort(valid_shape_points, chunk_size: 150000) do |shape_point|
        [ shape_point.id, shape_point.pt_sequence.to_i ]
      end

      current_shape = nil
      sorted_shape_points.each do |shape_point|
        shape_id = shape_point.id

        if current_shape.nil? || current_shape.id != shape_id
          yield current_shape if current_shape
          current_shape = Shape.new id: shape_id
        end

        current_shape.points << shape_point
      end

      yield current_shape if current_shape
    end

    def shapes
      enum_for(:each_shape)
    end

    def each_service
      build = ServiceBuilder.new(self)
      while next_service = build.next
        yield next_service
      end
    end

    class ServiceBuilder
      def initialize(source)
        @source = source
      end

      attr_reader :source

      def with_service_id(collection)
        collection.reject { |c| c.service_id.nil? }
      end

      def sorted_calendars
        @sorted_calendars ||= OfflineSort.sort with_service_id(source.calendars), chunk_size: 150000, &:service_id
      end

      def sorted_calendar_dates
        @sorted_calendar_dates ||= OfflineSort.sort with_service_id(source.calendar_dates), chunk_size: 250000, &:service_id
      end

      def next
        next_calendar_service_id = next_calendar&.service_id
        next_calendar_date_service_id = next_calendar_date&.service_id
     
        return next_with_calendar if next_calendar_service_id && (next_calendar_date_service_id.nil? || next_calendar_service_id <= next_calendar_date_service_id)
        return next_with_only_calendar_dates if next_calendar_date_service_id

        nil
      end


      def next_calendar
        sorted_calendars.peek
      rescue StopIteration
        nil
      end

      def next_calendar_date
        sorted_calendar_dates.peek
      rescue StopIteration
        nil
      end

      def next_with_calendar
        service = GTFS::Service.new(calendar: sorted_calendars.next)
  
        begin
          while sorted_calendar_dates.peek.service_id == service.service_id
            service.calendar_dates << sorted_calendar_dates.next
          end
        rescue StopIteration
          # ok
        end
  
        service
      end

      def next_with_only_calendar_dates
        service = GTFS::Service.new(calendar_dates: [sorted_calendar_dates.next])

        begin
          while sorted_calendar_dates.peek.service_id == service.service_id
            service.calendar_dates << sorted_calendar_dates.next
          end
        rescue StopIteration
          # ok
        end

        service
      end
    end

    def services
      enum_for(:each_service)
    end  

    def files
      @files ||= {}
    end

    def service_ids_on(date)
      calendar_day = [
        :sunday, :monday, :tuesday,
        :wednesday, :thursday, :friday, :saturday
      ][date.wday]

      target_calendars = calendars.select do |c|
        c.send(calendar_day) == '1' && c.date_range.include?(date)
      end

      gtfs_date = date.strftime("%Y%m%d")
      target_excluded_dates = calendar_dates.select { |d| d.date == gtfs_date && d.removed? }
      target_included_dates = calendar_dates.select { |d| d.date == gtfs_date && d.added? }

      (target_calendars.map(&:service_id) -
       target_excluded_dates.map(&:service_id) +
       target_included_dates.map(&:service_id)).uniq
    end

    def trips_on(date)
      service_ids = service_ids_on(date)
      trips.select { |t| service_ids.include? t.service_id }
    end

    def parse_file(filename)
      return [] unless entries.include?(filename)

      open File.join(@tmp_dir, '/', filename), 'r:bom|utf-8' do |f|
        files[filename] ||= yield f
      end
    end

    def validity_period
      # TODO: use feed_info date range when available
      services_validity_period
    end

    def services_validity_period
      min_date = max_date = nil
    
      calendars.each do |calendar|
        min_date = [ calendar.date_range&.min, min_date ].compact.min
        max_date = [ calendar.date_range&.max, max_date ].compact.max
      end

      calendar_dates.each do |calendar_date|
        date = calendar_date.ruby_date
        min_date = [ date, min_date ].compact.min
        max_date = [ date, max_date ].compact.max
      end

      min_date..max_date if min_date && max_date
    end
  end
end
