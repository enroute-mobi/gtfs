require 'gtfs/model/gtfs_attribute'
require 'gtfs/model/write_collection'
require 'gtfs/model/class_methods'

module GTFS
  module Model
    def self.included(base)
      base.extend ClassMethods

      base.class_variable_set('@@prefix', '')
      base.class_variable_set('@@attributes', [])

      def valid?
        !self.class.required_attrs.any?{|f| self.send(f.to_sym).nil?}
      end

      def initialize(attrs = {})
        attrs.each do |key, val|
          writer_method = "#{key}="
          send writer_method, val if respond_to?(writer_method)
        end
      end
    end

    def ==(other)
      self.class.attrs.all? do |attribute|
        other.respond_to?(attribute) && send(attribute) == other.send(attribute)
      end
    end

    def to_csv(columns)
      csv_values = []
      self.class.attrs.each do |attribute|
        next unless columns.include?(attribute)
        
        value = send(attribute) 
        csv_values << self.class.to_csv(value)
      end
      csv_values
    end

    def to_msgpack_ext
      self.class.attrs.map do |attribute|
        send(attribute).to_s
      end.to_msgpack
    end
  end
end
