[![Build Status](https://travis-ci.org/af83/gtfs.svg?branch=master)](https://travis-ci.org/af83/gtfs)

### GTFS Ruby

A Ruby wrapper for the [General Transit Feed Specification](https://developers.google.com/transit/gtfs/)

### Getting started

Initialize a new GTFS source:

    # Defaults to strict checking of required columns
    source = GTFS::Source.build(<URI or Path to GTFS zip file>)

    # Relax the column checks, useful for sources that don't conform to standard
    source = GTFS::Source.build(<URI or Path to GTFS zip file>, {strict: false})

Accessing GTFS data from the source:

    source.agencies
    source.stops
    source.routes
    source.trips
    source.stop_times
    source.calendars
    source.calendar_dates
    source.fare_attributes
    source.fare_rules
    source.shapes
    source.frequencies
    source.transfers

Alternatively:

    source.each_agency {|agency| puts agency}
    ...
    source.each_transfer {|transfer| puts transfer}

### Split GTFS file

With [gtfs-split](https://bitbucket.org/enroute-mobi/gtfs/src/master/exe/gtfs-split), it's possible to split a given GTFS file

By line:

```
$ bundle exec exe/gtfs-split --by=line gtfs.zip
```

By agency:

```
$ bundle exec exe/gtfs-split --by=agency gtfs.zip
```

The splitted GTFS files are creating (in the same directory) by using the agency or line identifier:

```
$ ls gtfs-*.zip
gtfs-10.zip
gtfs-12.zip
gtfs-21.zip
gtfs-23.zip
gtfs-A.zip
...
```

### GTFS trips statistics

With [gtfs-stats](https://bitbucket.org/enroute-mobi/gtfs/src/master/exe/gtfs-stats), it's possible to calculate trips statistics for a given GTFS file

```
be exe/gtfs-stats ~/Downloads/gtfs_beta.zip
Total Trip Count: 166047
Dataset from 2022-03-21 to 2022-08-21 (153 days)
Sunday Average Trips: 13335
Monday Average Trips: 19171
Tuesday Average Trips: 20042
Wednesday Average Trips: 20042
Thursday Average Trips: 20042
Friday Average Trips: 18911
Saturday Average Trips: 14815
Max Trip per day: 20142
```

### License

This project is licensed under the MIT license, a copy of which can be found in the LICENSE file.
