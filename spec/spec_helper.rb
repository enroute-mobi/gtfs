unless ENV['NO_RCOV']
  require 'simplecov'

  SimpleCov.start do
    add_filter 'vendor'
    add_filter 'spec'

    if ENV['CODACY_PROJECT_TOKEN']
      require 'simplecov-cobertura'
      formatter SimpleCov::Formatter::CoberturaFormatter
    end

    enable_coverage :branch
  end
end

require 'vcr'
require 'gtfs'

Dir["#{File.expand_path('../support',__FILE__)}/**/*.rb"].each { |f| require f }

VCR.configure do |config|
  config.cassette_library_dir = File.join(File.dirname(__FILE__), '/fixtures/cassettes')
  config.hook_into :webmock
  config.configure_rspec_metadata!
  config.ignore_hosts 'api.codacy.com'
end
