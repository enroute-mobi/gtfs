require 'spec_helper'

describe GTFS::ShapePoint do
  subject(:point) { GTFS::ShapePoint.new }

  describe 'ShapePoint.parse_shape_points' do
    let(:header_line) {"shape_id,shape_pt_lat,shape_pt_lon,shape_pt_sequence,shape_dist_traveled\n"}
    let(:invalid_header_line) {",,,shape_pt_sequence,shape_dist_traveled\n"}
    let(:valid_line) {"59135,39.354286,-76.662453,19,0.8136\n"}
    let(:invalid_line) {",39.354286,,,0.8136\n"}

    subject {GTFS::ShapePoint.parse_shape_points(source_text, opts)}

    include_examples 'models'
  end

  describe 'Shape.generate_shapes' do
    it "should produce the correct csv output" do
      csv = GTFS::ShapePoint.generate_csv do |shapes|
        shapes << {
          id: 'A_shp',
          pt_lat: 37.61956,
          pt_lon: -122.48161,
          pt_sequence: 1,
          dist_traveled: 0,
        }
      end
      csv.should eq("shape_id,shape_pt_lat,shape_pt_lon,shape_pt_sequence,shape_dist_traveled\n"+
      "A_shp,37.61956,-122.48161,1,0\n")
    end

    it "should filter dynamically unused csv columns" do
      csv = GTFS::ShapePoint.generate_csv do |shapes|
        shapes << {
          id: 'A_shp',
          pt_lat: 37.61956,
          pt_lon: -122.48161,
          pt_sequence: 1,
          dist_traveled: 0,
        }
        shapes << {
          id: 'A_shp',
          pt_lat: 37.61956,
          pt_lon: -122.48161,
          pt_sequence: 1,
        }
      end
      csv.should eq("shape_id,shape_pt_lat,shape_pt_lon,shape_pt_sequence,shape_dist_traveled\n"+
      "A_shp,37.61956,-122.48161,1,0\n"+
      "A_shp,37.61956,-122.48161,1,\n"
      )
    end
  end


  describe "#latitude" do
    subject { point.latitude }

    context "when pt_lat is '37.61956'" do 
      before { point.pt_lat = '37.61956' }
      it { is_expected.to eq(37.61956) }
      it { is_expected.to be_a(Float) }
    end
  end

  describe "#longitude" do
    subject { point.longitude }

    context "when pt_lon is '-122.48161'" do 
      before { point.pt_lon = '-122.48161' }
      it { is_expected.to eq(-122.48161) }
      it { is_expected.to be_a(Float) }
    end
  end
end
