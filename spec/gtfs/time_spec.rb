require 'spec_helper'

RSpec.describe GTFS::Time do

  describe '.create' do
    [
      [Time.new(2100,01,01,17), nil, GTFS::Time.new(17) ],
      [Time.new(2000,01,01,17,16,15), nil, GTFS::Time.new(17,16,15) ],
      [Time.new(2000,01,01,17), {day_offset: 1}, GTFS::Time.new(17, day_offset: 1)]
    ].each do |time, attributes, expected|
      attributes ||= {}
      it "creates #{expected.inspect} from #{time.class} #{time.inspect} with #{attributes.inspect}" do
        GTFS::Time.create(time, attributes).should eq(expected)
      end
    end

  end

  describe '#hour_with_day_offset' do

    it "includes hours from day_offset" do
      GTFS::Time.new(1, day_offset: 2).hour_with_day_offset.should eq(49)
    end

  end

  describe '.new' do

    context "when hour is higher than 23" do
      subject { GTFS::Time.new(49) }

      it "rounds the hour value" do
        subject.hour.should eq(1)
      end

      it "defines an implicit day offset" do
        subject.day_offset.should eq(2)
      end
    end

  end

  describe "#to_s" do

    [
      [ GTFS::Time.new(17), "17:00:00" ],
      [ GTFS::Time.new(17,01), "17:01:00" ],
      [ GTFS::Time.new(49), "49:00:00" ]
    ].each do |time, expected|
      it "returns #{expected.inspect} for #{time.inspect}" do
        time.to_s.should eq(expected)
      end
    end

  end

  describe '.parse' do

    [
      [ "17:00:00", GTFS::Time.new(17) ],
      [ "17:16:00", GTFS::Time.new(17,16) ],
      [ "17:16:15", GTFS::Time.new(17,16,15) ],
      [ "49:00:00", GTFS::Time.new(1,day_offset: 2) ],
      [ "17:00", GTFS::Time.new(17) ],
    ].each do |definition, expected|
      it "creates #{expected.inspect} for #{definition.inspect}" do
        GTFS::Time.parse(definition).should eq(expected)
      end
    end

  end

  describe '#<=>' do

    [
      [ "16:00", "17:00" ],
      [ "17:00", "17:01" ],
      [ "17:00:00", "17:00:01"  ],
      [ "23:59:59", "24:00:00"  ],
    ].each do |definition, other_definition|
      describe "#{definition}" do
        subject { GTFS::Time.parse(definition) }
        let(:other) { GTFS::Time.parse(other_definition) }

        it { is_expected.to be < other }
      end
    end

  end
end
