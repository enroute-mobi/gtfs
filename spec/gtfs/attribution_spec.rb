require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe GTFS::Attribution do
  subject(:model) { GTFS::Attribution.new }

  let(:header_line) {"attribution_id,agency_id,route_id,trip_id,organization_name,is_producer,is_operator,is_authority,attribution_url,attribution_email,attribution_phone\n"}
  let(:valid_line) {"1,1,2,AB1,Sample,0,1,0,http://www.mta.maryland.gov,test@test.com,0666666666\n"}

  describe 'Attribution.generate_attributions' do
    it "should produce the correct csv output" do
      csv = GTFS::Attribution.generate_csv do |attributions|
        attributions << {
          attribution_id: 1,
          agency_id: 1,
          route_id: 2,
          trip_id: 'AB1',
          organization_name: 'Sample',
          is_producer: 0,
          is_operator: 1,
          is_authority: 0,
          attribution_url: 'http://www.mta.maryland.gov',
          attribution_email: 'test@test.com',
          attribution_phone: '0666666666'
        }
      end

      csv.should eq(header_line  + valid_line)
    end
  end

  describe 'operator?' do
    subject { model.operator? }

    context 'when is_operator is "1"' do
      before { model.is_operator = '1' }

      it { is_expected.to be_truthy }
    end

    context 'when is_operator is "0"' do
      before { model.is_operator = '0' }

      it { is_expected.to be_falsy }
    end
  end

  describe 'producer?' do
    subject { model.producer? }

    context 'when is_producer is "1"' do
      before { model.is_producer = '1' }

      it { is_expected.to be_truthy }
    end

    context 'when is_producer is "0"' do
      before { model.is_producer = '0' }

      it { is_expected.to be_falsy }
    end
  end

  describe 'authority?' do
    subject { model.authority? }

    context 'when is_authority is "1"' do
      before { model.is_authority = '1' }

      it { is_expected.to be_truthy }
    end

    context 'when is_authority is "0"' do
      before { model.is_authority = '0' }

      it { is_expected.to be_falsy }
    end
  end
end
