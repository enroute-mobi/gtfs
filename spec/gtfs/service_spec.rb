# frozen_string_literal: true

require 'spec_helper'

RSpec.describe GTFS::Service do 
  subject(:service) { GTFS::Service.new }

  describe '#service_id' do 
    subject { service.service_id }

    context "when the given service_id is 'dummy'" do
      let(:service) { GTFS::Service.new(service_id: 'dummy') }
      it { is_expected.to eq('dummy') }
    end

    context "when the GTFS Calendar service_id is 'dummy'" do
      let(:service) { GTFS::Service.new(calendar: GTFS::Calendar.new(service_id: 'dummy')) }

      
      it { is_expected.to eq('dummy') }
    end

    context "when the GTFS Calendar Dates service_id is 'dummy'" do
      let(:service) { GTFS::Service.new(calendar_dates: [GTFS::CalendarDate.new(service_id: 'dummy')]) }

      
      it { is_expected.to eq('dummy') }
    end
  end
end