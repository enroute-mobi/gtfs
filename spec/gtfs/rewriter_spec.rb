require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe GTFS::UniquenessTarget::Collection do
  describe "#push" do
    subject { collection.push resource }

    context "when resource is Stop" do
      let(:resource) { GTFS::Stop.new id: "test" }
      let(:target) { double(stops: []) }
      let(:collection) { described_class.new(:stops, target) }

      context "when another Stop with different id has been already pushed" do
        let(:another_resource) { GTFS::Stop.new id: "another" }
        before { collection.push another_resource }

        it "add the resource to the target" do
          expect { subject }.to change(target, :stops).from([another_resource]).to([another_resource, resource])
        end
      end

      context "when another Stop with same id has been already pushed" do
        let(:another_resource) { resource.dup }
        before { collection.push another_resource }

        it "doesn't add the resource to the target" do
          expect { subject }.to_not change(target, :stops)
        end
      end
    end

    context "when resource is CalendarDate" do
      let(:resource) { GTFS::CalendarDate.new service_id: "test", date: "20300101" }
      let(:target) { double(calendar_dates: []) }
      let(:collection) { described_class.new(:calendar_dates, target) }

      context "when another CalendarDate with same service_id and different date has been already pushed" do
        let(:another_resource) { resource.dup.tap { |r| r.date = "20301231" } }
        before { collection.push another_resource }

        it "add the resource to the target" do
          expect { subject }.to change(target, :calendar_dates).from([another_resource]).to([another_resource, resource])
        end
      end

      context "when another CalendarDate with same service_id and same date has been already pushed" do
        let(:another_resource) { resource.dup }
        before { collection.push another_resource }

        it "doesn't add the resource to the target" do
          expect { subject }.to_not change(target, :calendar_dates)
        end
      end
    end

    context "when resource is StopTime" do
      let(:resource) { GTFS::StopTime.new trip_id: "test", stop_sequence: 10 }
      let(:target) { double(stop_times: []) }
      let(:collection) { described_class.new(:stop_times, target) }

      context "when another StopTime with the same trip_id and different stop sequence has been already pushed" do
        let(:another_resource) { resource.dup.tap { |r| r.stop_sequence += 1 } }
        before { collection.push another_resource }

        it "add the resource to the target" do
          expect { subject }.to change(target, :stop_times).from([another_resource]).to([another_resource, resource])
        end
      end

      context "when another StopTime with a differnt trip_id and stop sequence has been already pushed" do
        let(:another_resource) { resource.dup.tap { |r| r.trip_id = "another" ; r.stop_sequence += 1 } }
        before { collection.push another_resource }

        it "add the resource to the target" do
          expect { subject }.to change(target, :stop_times).from([another_resource]).to([another_resource, resource])
        end
      end

      context "when another StopTime with same trip_id and stop sequence has been already pushed" do
        let(:another_resource) { resource.dup }
        before { collection.push another_resource }

        it "doesn't add the resource to the target" do
          expect { subject }.to_not change(target, :stop_times)
        end
      end
    end
  end
end
