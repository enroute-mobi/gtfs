require "spec_helper"

RSpec.describe GTFS::Model::WriteCollection do

  let(:model) { GTFS::Trip.new route_id: 'A', service_id: 'WE', id: 'AWE1', headsign: 'Downtown' }

  subject(:write_collection) { GTFS::Model::WriteCollection.new(model.class) }

  def collection_content
    write_collection.collection.to_enum.to_a
  end

  describe "#push" do

    it "adds the specified model to the collection" do
      write_collection.push model
      expect(collection_content).to eq([model])
    end

  end

end

RSpec.describe GTFS::Model::FileCollection do

  def model(index)
    GTFS::Trip.new route_id: 'A', service_id: "WE#{index}", id: "AWE#{index}", headsign: 'Downtown'
  end

  subject(:collection) { GTFS::Model::FileCollection.new }

  describe ".each" do

    it "retrieves each model pushed in the collection" do
      1000.times do |n|
        collection.push model(n)
      end

      collection.each_with_index do |model, n|
        expect(model).to eq(model(n))
      end
    end

  end

end

RSpec.describe GTFS::Model::MemoryCollection do

  def model(index)
    GTFS::Trip.new route_id: 'A', service_id: "WE#{index}", id: "AWE#{index}", headsign: 'Downtown'
  end

  subject(:collection) { GTFS::Model::MemoryCollection.new }

  describe ".each" do

    it "retrieves each model pushed in the collection" do
      1000.times do |n|
        collection.push model(n)
      end

      collection.each_with_index do |model, n|
        expect(model).to eq(model(n))
      end
    end

  end

end

RSpec.describe GTFS::Model::AutoCollection do

  def model(index)
    GTFS::Trip.new route_id: 'A', service_id: "WE#{index}", id: "AWE#{index}", headsign: 'Downtown'
  end

  let(:model_count) { 1000 }

  subject(:collection) { GTFS::Model::AutoCollection.new GTFS::Trip, maximum_size: model_count / 2 }

  describe ".each" do

    it "retrieves each model pushed in the collection" do
      model_count.times do |n|
        collection.push model(n)
      end

      collection.each_with_index do |model, n|
        expect(model).to eq(model(n))
      end
    end

  end

end
