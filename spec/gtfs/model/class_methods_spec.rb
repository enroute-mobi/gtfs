require "spec_helper"

RSpec.describe GTFS::Model::ClassMethods do
  let(:gtfs_model) { GTFS::Agency }

  describe '.parse_model' do
    subject { gtfs_model.parse_model(attributes) }

    context 'when attribute name contains a non-alphanumeric character (e.g "% - name")' do
      let(:attributes) { { "% - name" => "value" } }

      it { is_expected.to have_attributes(name: "value") }
    end

    context 'when one of the attribute names is nil (e.g. name: "dummy", nil: nil)' do
      let(:attributes) { { name: 'dummy', nil => nil } }

      it { is_expected.to have_attributes(name: "dummy") }
    end
  end
end
