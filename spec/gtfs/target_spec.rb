require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe GTFS::Target do
  describe 'Target.open' do
    def test_entity(entity, map)
      map.each do |k,v|
        entity.send(k).should eq(v)
      end
    end

    it "should produce the correct export zip" do
      tmp_dir = Dir.mktmpdir
      zip_path = File.join(tmp_dir, '/test.zip')

      first_agency = {
        id: '1',
        name: 'MTA',
        url: 'http://www.mta.maryland.gov',
        timezone: 'America/New_York',
        lang: 'en',
        phone: '410-539-5000'
      }
      second_agency = {
        id: '2',
        name: 'MTA2',
        url: 'http://www.mta.maryland.gov/2',
        timezone: 'America/New_York',
        lang: 'en',
        phone: '410-539-5001'
      }
      stop_time = {
          trip_id: 'AWE1',
          arrival_time: '0:06:10',
          departure_time: '0:06:10',
          stop_id: 'S1',
          stop_sequence: '1',
          pickup_type: '0',
          drop_off_type: '0',
        }

      GTFS::Target.open(zip_path) do |target|
        target.agencies << first_agency
        target.agencies << second_agency
        target.stop_times << stop_time
      end

      Zip::File.open(zip_path) do |zip|
        zip.entries.length.should eq(2)
      end

      source = GTFS::Source.build(zip_path)
      source.agencies.length.should eq(2)

      test_entity source.agencies.first, first_agency
      test_entity source.agencies[1], second_agency

      source.stop_times_count.should eq(1)
      test_entity source.stop_times.first, stop_time
    end
  end

  describe ".shapes" do

    let(:target) { GTFS::Target.new('dummy.zip') }
    let(:expected_csv) do
      content = <<-CSV
        shape_id,shape_pt_lat,shape_pt_lon,shape_pt_sequence
        test,39.350719,-76.660767,0
        test,39.350826,-76.662621,1
      CSV
      content.gsub(/^ */,'')
    end

    it "adds a ShapePoint for each point" do
      shape = GTFS::Shape.new(id: 'test')
      shape.points << GTFS::ShapePoint.new(latitude: 39.350719,longitude: -76.660767)
      shape.points << GTFS::ShapePoint.new(latitude: 39.350826,longitude: -76.662621)
      target.shapes << shape

      csv_content = StringIO.new
      csv = CSV.new(csv_content)
      target.shape_points.array_to_csv(csv)
      csv.close
      csv_content.string.should eq(expected_csv)
    end

  end

  describe '#add' do
    let(:target) { GTFS::Target.new('dummy.zip') }

    context 'when given resource is a GTFS::Stop' do
      let(:resource) { GTFS::Stop.new }

      it 'should add the resource in stops collection' do
        target.add resource
        expect(target.stops).to include(resource)
      end
    end

    context 'when given resource is a GTFS::Line' do
      let(:resource) { GTFS::Route.new }

      it 'should add the resource in stops collection' do
        target.add resource
        expect(target.routes).to include(resource)
      end
    end
  end
end
