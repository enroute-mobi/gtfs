require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe GTFS::Source do
  let(:valid_local_source) do
    File.expand_path(File.dirname(__FILE__) + '/../fixtures/valid_gtfs.zip')
  end

  describe '#build' do
    let(:opts) {{}}
    let(:data_source) {valid_local_source}
    subject {GTFS::Source.build(data_source, opts)}

    context 'with a url as a data root', vcr: { :cassette_name => 'valid_gtfs_uri' } do
      let(:data_source) {'http://dl.dropbox.com/u/416235/work/valid_gtfs.zip'}

      it {should be_instance_of GTFS::URLSource}
      it { is_expected.to have_attributes(options: GTFS::Source::DEFAULT_OPTIONS) }
    end

    context 'with a file path as a data root' do
      let(:data_source) {valid_local_source}

      it {should be_instance_of GTFS::LocalSource}
      it { is_expected.to have_attributes(options: GTFS::Source::DEFAULT_OPTIONS) }
    end

    context 'with a file object as a data root' do
      let(:data_source) {File.open(valid_local_source)}

      it {should be_instance_of GTFS::LocalSource}
      it { is_expected.to have_attributes(options: GTFS::Source::DEFAULT_OPTIONS) }
    end

    context 'with options to disable strict checks' do
      let(:opts) {{strict: false}}
      it { is_expected.to have_attributes(options: opts) }
    end
  end

  describe '#new(source)' do
    it 'should not allow a base GTFS::Source to be initialized' do
      -> { GTFS::Source.new(valid_local_source) }.should raise_exception
    end
  end

  describe '#agencies' do
    subject {source.agencies}

    context 'when the source has agencies' do
      let(:source) {GTFS::Source.build(valid_local_source)}

      it {should_not be_empty}
      it "provides GTFS::Agency models" do
        expect(subject.first).to be_an_instance_of(GTFS::Agency)
      end
    end
  end

  describe '#agencies_count' do
    subject {source.agencies_count}

    context 'when the source has agencies' do
      let(:source) {GTFS::Source.build(valid_local_source)}

      it {should eq 1}
    end
  end

  describe '#stops' do
  end

  describe '#routes' do
  end

  describe '#trips' do
  end

  describe '#stop_times' do
  end

  describe '#each_trip_with_stop_times' do

    let(:expected_trip_ids) do
      %w{1075508 1075509 1075510 1075511 1075512 1075513 1075514 1075515 1075516}
    end

    let(:expected_arrival_times_by_trip) do
      {
        "1075513" =>
        %w{5:34:00 5:34:26 5:34:55 5:35:24 5:36:00 5:37:00 5:37:14 5:38:00 5:38:49},
        "1075508" =>
        %w{4:34:00 4:34:26 4:34:55 4:35:24 4:36:00 4:37:00 4:37:14 4:38:00 4:38:49}
      }
    end

    let(:source) {GTFS::Source.build(valid_local_source)}

    it "provides each trip (even empty ones)" do
      trip_ids = []
      source.each_trip_with_stop_times do |trip, _|
        trip_ids << trip.id
      end

      trip_ids.should eq(expected_trip_ids)
    end

    it "provides stop times for each trip" do
      trip_ids = []
      source.each_trip_with_stop_times do |trip, stop_times|
        trip_ids << trip.id
        expected_arrival_times = expected_arrival_times_by_trip.fetch(trip.id, [])
        stop_times.map(&:arrival_time).should eq(expected_arrival_times)
      end

      expect(trip_ids).to match_array(%w[1075508 1075509 1075510 1075511 1075512 1075513 1075514 1075515 1075516])
    end
  end

  describe '#shape_points' do

    let(:source) {GTFS::Source.build(valid_local_source)}

    it "provides a ShapePoint for each line in the shapes.txt file" do
      source.shape_points.count.should eq(18)
    end

  end

  describe '#shapes' do

    let(:source) { GTFS::Source.build(valid_local_source) }

    it "provides a Shape for each shape_id in the shapes.txt file" do
      source.shapes.count.should eq(2)
      source.shapes.map(&:id).should eq(%w{63542 63543})
      source.shapes.map { |s| s.points.count  }.should eq([9, 9])
    end

  end

  describe '#services' do
    subject(:services) { source.services }

    context "with GTFS sample" do
      let(:source) { GTFS::Source.build(valid_local_source) }

      it "provides a Service for each service_id in the calendars.txt file" do
        expect(subject).to contain_exactly(
                             an_object_having_attributes(service_id: '1',
                                                         calendar_dates: a_collection_containing_exactly(an_object_having_attributes(service_id: '1', date: '20120528'))),
                             an_object_having_attributes(service_id: '2',
                                                         calendar_dates: an_empty_collection),
                             an_object_having_attributes(service_id: '3',
                                                         calendar_dates: a_collection_containing_exactly(an_object_having_attributes(service_id: '3', date: '20120528'))),
                             an_object_having_attributes(service_id: '4',
                                                         calendar_dates: a_collection_containing_exactly(
                                                           an_object_having_attributes(service_id: '4', date: '20120527'),
                                                           an_object_having_attributes(service_id: '4', date: '20120528')
                                                         )
                                                        )
                           )
      end
    end

    context "when Source contains a GTFS Calendar without service_id" do
      subject { services.to_a }
      # TODO: should use an empty Source
      let(:source) { GTFS::Source.build(valid_local_source) }

      before do
        allow(source).to receive(:calendars).and_return([GTFS::Calendar.new(service_id: '1'), GTFS::Calendar.new ])
        allow(source).to receive(:calendar_dates).and_return([])
      end

      it { is_expected.to a_collection_containing_exactly(an_object_having_attributes(service_id: '1')) }
    end

    context "when Source contains a GTFS Calendar Date without service_id" do
      subject { services.to_a }
      # TODO: should use an empty Source
      let(:source) { GTFS::Source.build(valid_local_source) }

      before do
        allow(source).to receive(:calendars).and_return([])
        allow(source).to receive(:calendar_dates).and_return([GTFS::CalendarDate.new(service_id: '1'), GTFS::CalendarDate.new ])
      end

      it { is_expected.to a_collection_containing_exactly(an_object_having_attributes(service_id: '1')) }
    end
  end

  describe '#extract_to_cache with glob search file' do
    let(:glob_local_source) do
      File.expand_path(File.dirname(__FILE__) + '/../fixtures/gtfs-40.zip')
    end

    let(:source) { GTFS::Source.build(glob_local_source) }

    describe '#agencies' do
      subject { source.agencies.first }

      it { is_expected.to be_an_instance_of(GTFS::Agency) }
    end

    describe '#stops' do
      subject { source.stops.first }

      it { is_expected.to be_an_instance_of(GTFS::Stop) }
    end

    describe '#routes' do
      subject { source.routes.first }

      it { is_expected.to be_an_instance_of(GTFS::Route) }
    end
  end
end
